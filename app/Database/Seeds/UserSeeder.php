<?php namespace App\Database\Seeds;

use CodeIgniter\I18n\Time;

class UserSeeder extends \CodeIgniter\Database\Seeder
{

        public function run()
        {
          $passHash1 = password_hash('Faradisa', PASSWORD_DEFAULT);
          $passHash2 = password_hash('Admin', PASSWORD_DEFAULT);
          $faker = \Faker\Factory::create('id_ID');
          $UID = uniqid();

              for ($i=0; $i < 20; $i++) {

                $data = [
                        'UID' => $UID.$i,
                        'fullName'    => $faker->name,
                        'POB'         => $faker->city,
                        'DOB'         => Time::createFromTimestamp($faker->unixTime()),
                        'gender'      => 'Male',
                        'email'       => $faker->email,
                        'mobile'      => $faker->e164PhoneNumber,
                        'passwd'      => $passHash1,
                        'status'      => 'Active',
                        ];

                // Using Query Builder
                $this->db->table('user_tbl')->insert($data);
              }
        }
}
