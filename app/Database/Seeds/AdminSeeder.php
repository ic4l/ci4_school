<?php namespace App\Database\Seeds;

use CodeIgniter\I18n\Time;

class AdminSeeder extends \CodeIgniter\Database\Seeder
{

        public function run()
        {
          $passHash2 = password_hash('Admin', PASSWORD_DEFAULT);
          $UID = uniqid();

                $data = [
                        'UID' => $UID,
                        'fullName'    => 'Admin',
                        'POB'         => 'Jeddah',
                        'DOB'         => '1986-08-07',
                        'gender'      => 'Male',
                        'email'       => 'admin@mail.com',
                        'mobile'      => '06288888888888',
                        'passwd'      => $passHash2,
                        'status'      => 'Active',
                ];

                // Using Query Builder
                $this->db->table('user_tbl')->insert($data);
        }
}
