<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UserTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
      'id'           => [
           'type'           => 'INT',
           'constraint'     => 11,
           'unsigned'       => TRUE,
           'auto_increment' => TRUE
        ],
				'UID'      		 => [
            'type'           => 'VARCHAR',
            'constraint'     => '255',
						'unique'			=> TRUE,
        ],
				'fullName'     => [
            'type'           => 'VARCHAR',
            'constraint'     => '255',
        ],
				'POB'      		 => [
            'type'           => 'VARCHAR',
            'constraint'     => '64',
        ],
				'DOB'       	 => [
            'type'           => 'DATE',
        ],
				'gender'       => [
            'type'           => 'VARCHAR',
            'constraint'     => '24',
        ],
				'email'				 => [
             'type'           => 'VARCHAR',
						 'constraint'     => '255',
						 'unique'					=> TRUE,
        ],
				'mobile'    	 => [
             'type'           => 'VARCHAR',
						 'constraint'     => '255',
        ],
				'passwd'       => [
            'type'           => 'VARCHAR',
            'constraint'     => '255',
        ],
				'role'      	 => [
            'type'           => 'VARCHAR',
            'constraint'     => '16',
						'default'				 =>  'Default'
        ],
				'status'       => [
            'type'           => 'ENUM',
            'constraint'     => '"Active", "Inactive"',
						'default'				 =>  'Active'
        ],
				'image'      	 => [
            'type'           => 'VARCHAR',
            'constraint'     => '255',
						'default'				 =>  'default.png'
        ],
				'created_at'   => [
            'type'           => 'DATE',
        ],
				'updated_at'   => [
            'type'           => 'DATE',
        ],
  ]);
  $this->forge->addKey('id', TRUE);
  $this->forge->createTable('user_tbl');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('user_tbl');
	}

	//--------------------------------------------------------------------
}
