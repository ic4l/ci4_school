<!doctype html>
<html lang="en">
  <head>
    <!-- Start Header -->
    <?= view('_template/_main/header') ?>
    <!-- End  Header -->
    <!-- Start CSS -->
    <?= view('_template/_main/css') ?>
    <!-- End CSS -->
  </head>

  <body>
    <!-- START NavBar Header -->
    <?= view('_template/_main/navbar') ?>
    <!-- END NavBar Header -->

    <!-- BEGIN Content  ------------------------------------------------------------------------------------------------->
    <?= $this->renderSection('content') ?>
    <!-- End Content  --------------------------------------------------------------------------------------------------->

    <!-- Start JS -->
  	<?= view('_template/_main/js') ?>
  	<!-- End JS --></body>
</html>
