<?php $uri = service('uri'); ?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="<?= base_url(''); ?>">cSchool</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item <?= ($uri->getSegment(1) == '' ? 'active' : null)  ?>">
        <a class="nav-link" href="<?= base_url(''); ?>">Home</span></a>
      </li>
      <li class="nav-item dropdown <?= ($uri->getSegment(1) == 'user' ? 'active open' : null)  ?>">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          User
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?= base_url('user/create'); ?>">Create</a>
          <a class="dropdown-item" href="<?= base_url('user/read'); ?>">Read</a>
          <a class="dropdown-item disabled" href="<?= base_url('user/update'); ?>">Update</a>
          <a class="dropdown-item disabled" href="<?= base_url('user/delete'); ?>">Delete</a>
        </div>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
    <button class="btn btn-outline-primary my-2 my-sm-0" type="button">Login</button>
  </div>
</nav>
