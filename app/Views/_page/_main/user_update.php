<?= $this->extend('_template/_main/template') ?>

<?= $this->section('content') ?>
<div class="container-fluid mt-3">
  <h1>User Update</h1>
  <?= form_open(base_url('user/update')) ?>
    <div class="form-row">
      <div class="form-group col-md-4">
        <label for="inputAddress">Full Name</label>
        <input name="xFullname" type="text" value="<?= $xUser['fullName'] ?>" class="form-control" id="inputAddress" placeholder="Full Name" autofocus>
      </div>
      <div class="form-group col-md-4">
        <label for="inputEmail4">Place of Birth</label>
        <input name="xPOB" type="text" value="<?= $xUser['POB'] ?>" class="form-control" id="inputEmail4" placeholder="Place of Birth">
      </div>
      <div class="form-group col-md-4">
        <label for="inputEmail4">Date of Birth</label>
        <input name="xDOB" type="text" value="<?= $xUser['DOB'] ?>" class="form-control" id="inputEmail4" placeholder="Date of Birth">
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-3">
        <label for="inputEmail4">Email</label>
        <input name="xEmail" type="email" value="<?= $xUser['email'] ?>" class="form-control" id="inputEmail4" placeholder="Email">
      </div>
      <div class="form-group col-md-3">
        <label for="inputEmail4">Mobile</label>
        <input name="xMobile" type="text" value="<?= $xUser['mobile'] ?>" class="form-control" id="inputEmail4" placeholder="Mobile">
      </div>
      <div class="form-group col-md-3">
        <label for="inputGnder">Gender</label>
        <select name="xGender" id="inputGnder" class="form-control">
          <option selected><?= $xUser['gender'] ?></option>
          <option>Male</option>
          <option>Female</option>
        </select>
      </div>
      <div class="form-group col-md-3">
        <label for="inputRole">Role</label>
        <select name="xRole" id="inputRole" class="form-control">
          <option selected><?= $xUser['role'] ?></option>
          <option>Admin</option>
          <option>Principle</option>
          <option>Teacher</option>
          <option>Staff</option>
          <option>Student</option>
          <option>Parent</option>
          <option>Guardian</option>
        </select>
      </div>
    </div>
    <input type="hidden" name="xUID" value="<?= $xUser['UID'] ?>">
  <button type="submit" class="btn btn-warning">Update</button>
  <a href="<?= base_url('/user/read'); ?>" class="btn btn-primary">Cancel</a>
<?= form_close(); ?>
</div>
<?= $this->endSection() ?>
