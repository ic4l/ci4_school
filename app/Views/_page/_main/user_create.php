<?= $this->extend('_template/_main/template') ?>

<?= $this->section('content') ?>
<div class="container-fluid mt-3">
  <h1>User Create</h1>
  <?= form_open(base_url('user/create')) ?>
    <div class="form-row">
      <div class="form-group col-md-4">
        <label for="inputAddress">Full Name</label>
        <input name="xFullname" type="text" class="form-control" id="inputAddress" placeholder="Full Name" autofocus>
      </div>
      <div class="form-group col-md-4">
        <label for="inputEmail4">Place of Birth</label>
        <input name="xPOB" type="text" class="form-control" id="inputEmail4" placeholder="Place of Birth">
      </div>
      <div class="form-group col-md-4">
        <label for="inputEmail4">Date of Birth</label>
        <input name="xDOB" type="text" class="form-control" id="inputEmail4" placeholder="Date of Birth">
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-4">
        <label for="inputEmail4">Email</label>
        <input name="xEmail" type="email" class="form-control" id="inputEmail4" placeholder="Email">
      </div>
      <div class="form-group col-md-4">
        <label for="inputPassword4">Password</label>
        <input name="xPasswd" type="password" class="form-control" id="inputPassword4" placeholder="Password">
      </div>
      <div class="form-group col-md-4">
        <label for="inputPassword4">Confirm Password</label>
        <input name="cPasswd" type="password" class="form-control" id="inputPassword4" placeholder="Password">
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-4">
        <label for="inputEmail4">Mobile</label>
        <input name="xMobile" type="text" class="form-control" id="inputEmail4" placeholder="Mobile">
      </div>
      <div class="form-group col-md-4">
        <label for="inputGnder">Gender</label>
        <select name="xGender" id="inputGnder" class="form-control">
          <option selected>Choose...</option>
          <option>Male</option>
          <option>Female</option>
        </select>
      </div>
      <div class="form-group col-md-4">
        <label for="inputRole">Role</label>
        <select name="xRole" id="inputRole" class="form-control">
          <option selected>Choose...</option>
          <option>Admin</option>
          <option>Principle</option>
          <option>Teacher</option>
          <option>Staff</option>
          <option>Student</option>
          <option>Parent</option>
          <option>Guardian</option>
        </select>
      </div>
    </div>
  <button type="submit" class="btn btn-primary">Submit</button>
<?= form_close(); ?>
</div>
<?= $this->endSection() ?>
