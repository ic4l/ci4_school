<?= $this->extend('_template/_main/template') ?>

<?= $this->section('content') ?>
<div class="container-fluid mt-3">
  <h1>User Delete</h1>
  <?= form_open(base_url('user/delete')) ?>
    <div class="form-row">
      <div class="form-group col-md-4">
        <label for="inputAddress">Full Name</label>
        <input name="xFullname" type="text" value="<?= $xUser['fullName'] ?>" class="form-control" id="inputAddress" placeholder="Full Name" readonly autofocus>
      </div>
      <div class="form-group col-md-4">
        <label for="inputEmail4">Place of Birth</label>
        <input name="xPOB" type="text" value="<?= $xUser['POB'] ?>" class="form-control" id="inputEmail4" placeholder="Place of Birth" readonly>
      </div>
      <div class="form-group col-md-4">
        <label for="inputEmail4">Date of Birth</label>
        <input name="xDOB" type="text" value="<?= $xUser['DOB'] ?>" class="form-control" id="inputEmail4" placeholder="Date of Birth" readonly>
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-3">
        <label for="inputEmail4">Email</label>
        <input name="xEmail" type="email" value="<?= $xUser['email'] ?>" class="form-control" id="inputEmail4" placeholder="Email" readonly>
      </div>
      <div class="form-group col-md-3">
        <label for="inputEmail4">Mobile</label>
        <input name="xMobile" type="text" value="<?= $xUser['mobile'] ?>" class="form-control" id="inputEmail4" placeholder="Mobile" readonly>
      </div>
      <div class="form-group col-md-3">
        <label for="inputEmail4">Gender</label>
        <input name="xMobile" type="text" value="<?= $xUser['gender'] ?>" class="form-control" id="inputEmail4" placeholder="Gender" readonly>
      </div>
      <div class="form-group col-md-3">
        <label for="inputEmail4">Role</label>
        <input name="xMobile" type="text" value="<?= $xUser['role'] ?>" class="form-control" id="inputEmail4" placeholder="Role" readonly>
      </div>
    </div>
    <input type="hidden" name="xUID" value="<?= $xUser['UID'] ?>">
  <button type="submit" class="btn btn-danger">Delete</button>
  <a href="<?= base_url('/user/read'); ?>" class="btn btn-primary">Cancel</a>
<?= form_close(); ?>
</div>
<?= $this->endSection() ?>
