<?= $this->extend('_template/_main/template') ?>

<?= $this->section('content') ?>
<!-- START Main Content -->
<div class="container-fluid mt-3">
  <h1>User Read</h1>
  <?php if (session()->getFlashdata('xMSG')): ?>
    <div class="col-12">
      <div class="alert alert-primary" role="alert">
        Data Succesfully
        <strong><?= session()->getFlashdata('xMSG') ?></strong>
      </div>
    </div>
  <?php endif; ?>
  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">UID</th>
        <th scope="col">Full Name</th>
        <th scope="col">e-mail</th>
        <th scope="col">POB</th>
        <th scope="col">DOB</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($xUser as $x): ?>
      <tr>
        <th scope="row"><?= $x['UID'] ?></th>
        <td><?= $x['fullName'] ?></td>
        <td><?= $x['email'] ?></td>
        <td><?= $x['POB'] ?></td>
        <td><?= $x['DOB'] ?></td>
        <td>
          <a href="<?= base_url('user/update/'.$x['UID']); ?>" class="btn btn-warning">Edit</a>
          <a href="<?= base_url('user/delete/'.$x['UID']); ?>" class="btn btn-danger">Delete</a>
        </td>
      </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
<!-- END Main Content -->
<?= $this->endSection() ?>
