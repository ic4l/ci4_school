<?php namespace App\Controllers;

use App\Models\CrudModel;

class User extends BaseController
{
	protected $CrudModel;

	public function __construct()
	{
		$data = [];
		helper(['form', 'url']);
		$this->CrudModel = new CrudModel();
	}

	public function create()
	{
		if($this->request->getMethod() == 'post')
		{
			$table = 'user_tbl';
			$passHash = password_hash($this->request->getPost('xPasswd'), PASSWORD_DEFAULT);

			$data = [
				'UID'				=> uniqid(),
				'fullName'	=> mb_strtoupper($this->request->getPost('xFullname')),
				'email'			=> mb_strtolower($this->request->getPost('xEmail')),
				'passwd'		=> $passHash,
				'POB'			=> $this->request->getPost('xPOB'),
				'DOB'			=> $this->request->getPost('xDOB'),
				'mobile'			=> $this->request->getPost('xMobile'),
				'gender'			=> $this->request->getPost('xGender'),
				'role'			=> $this->request->getPost('xRole'),
			];

			$this->CrudModel->insert_data($table, $data);
			$session = session();
			$session->setFlashdata('xMSG', 'Created');
			return redirect()->to(base_url('user/read'));
		}
		return view('_page/_main/user_create');
	}

	public function read()
	{
		$table	= 'user_tbl';

		$data		= [
			'xUser' => $this->CrudModel->get_all($table),
		];

		return view('_page/_main/user_read', $data);
	}

	public function update($xUID = '')
	{
		$table	= 'user_tbl';
		$where	= 'UID';

		if($this->request->getMethod() == 'post')
		{
			$UID = $this->request->getPost('xUID');
			$data = [
				'fullName'	=> mb_strtoupper($this->request->getPost('xFullname')),
				'email'			=> mb_strtolower($this->request->getPost('xEmail')),
				'POB'			=> $this->request->getPost('xPOB'),
				'DOB'			=> $this->request->getPost('xDOB'),
				'mobile'			=> $this->request->getPost('xMobile'),
				'gender'			=> $this->request->getPost('xGender'),
				'role'			=> $this->request->getPost('xRole'),
			];

			$this->CrudModel->update_data($table, $UID, $data);
			$session = session();
			$session->setFlashdata('xMSG', 'Updated');
			return redirect()->to(base_url('user/read'));
		}

		$data		= [
			'xUser' => $this->CrudModel->get_where($table, $where, $xUID),
		];

		return view('_page/_main/user_update', $data);
	}

	public function delete($xUID = '')
	{
		$table	= 'user_tbl';
		$where	= 'UID';

		if($this->request->getMethod() == 'post')
		{
			$UID = $this->request->getPost('xUID');
			// $data = [
			// 	'role'			=> $this->request->getPost('xRole')
			// ];

			$this->CrudModel->delete_data($table, $where, $UID);
			$session = session();
			$session->setFlashdata('xMSG', 'Deleted');
			return redirect()->to(base_url('user/read'));
		}

		$data		= [
			'xUser' => $this->CrudModel->get_where($table, $where, $xUID),
		];

		return view('_page/_main/user_delete', $data);
	}

	//--------------------------------------------------------------------

}
